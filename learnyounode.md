### <span style='color: #3A539B; font-size:2.5em;'>	___NodeJS___ </span> ###
---
<span style='color: #F9690E; font-size:1.5em;'>__Learning how to use NodeJS in a nutshell__ </span>

`Author`

```
MAKDOUD Nizam
makdoudnizam@gmail.com
Don't hesitate if you have any question.
Version 0.1 (just a try, not definitive)
Good lecture !
```


a simple cheat sheet of  https://github.com/maxogden/art-of-node

Node.js is an open source project designed to help you write JavaScript programs that talk to networks, file systems or other I/O (input/output, reading/writing) sources.
## <span style='color: #3A539B; font-size:1.5em;'>	___Basis___ </span> ##
---

## <span style='color: #3A539B; font-size:1.5em;'>	___Core modules___ </span> ##
---
1. `fs` module: file systems
2. `networks` module : net (TCP), http, dgram (UDP)
3. `os` : module : OS specific information like the tmpdir location
4. `buffer`: module for allocating binary chunks of memory
5. `url, querystring, path`: modules for parsing urls and paths

Node handles I/O with: callbacks, events, streams and modules


### <span style='color: #03A678; font-size: 1.3em;'> _Callback_ </span> ###
---
Callbacks are functions that are executed asynchronously, or at a later time. Instead of the code reading top to bottom procedurally, async programs may execute different functions at different times based on the order and speed that earlier functions like http requests or file system reads happen.


### <span style='color: #03A678; font-size: 1.3em;'> _Events_ </span> ###
---
Much of the Node.js core API is built around an idiomatic asynchronous event-driven architecture in which certain kinds of objects (called `"emitters"`) periodically emit named events that cause Function objects ("`listeners"`) to be called.
All objects that emit events are instances of the `EventEmitter class`. These objects expose an `eventEmitter.on()` function that allows one or more Functions to be attached to named events emitted by the object. Typically, event names are camel-cased strings but any valid JavaScript property key can be used.
When the EventEmitter object emits an event, all of the Functions attached to that specific event are called synchronously. Any values returned by the called listeners are ignored and will be discarded.

### <span style='color: #03A678; font-size: 1.3em;'> _Stream_ </span> ###
---
From : https://github.com/substack/stream-handbook#introduction
The node stream module's primary composition operator is called `.pipe()`

`.pipe()` is just a function that takes a readable source stream src and hooks the output to a destination writable stream dst

```
src.pipe(dst)
```
#### _readable streams_ ####
---
Readable streams produce data that can be fed into a writable, transform, or duplex stream by calling .pipe():

```
readableStream.pipe(dst)
```



rs.push(null) tells the consumer that rs is done outputting data.

__Consuming a readable stream__


Most of the time it's much easier to just pipe a readable stream into another kind of stream or a stream created with a module like through or concat-stream, but occasionally it might be useful to consume a readable stream directly.


#### _writable streams_ ####
---

Just define a ._write(chunk, enc, next) function and then you can pipe a readable stream in:


```
var Writable = require('stream').Writable;
var ws = Writable();
ws._write = function (chunk, enc, next) {
    console.dir(chunk);
    next();

process.stdin.pipe(ws);
};
```

The first argument, `chunk` is the data that is written by the producer.

The second argument `enc` is a string with the string encoding, but only when opts.decodeString is false and you've been written a string.

The third argument, `next(err)`` is the callback that tells the consumer that they can write more data. You can optionally pass an error object err, which emits an 'error' event on the stream instance.

If the readable stream you're piping from writes strings, they will be converted into Buffers unless you create your writable stream with Writable({ decodeStrings: false }).

If the readable stream you're piping from writes objects, create your writable stream with Writable({ objectMode: true }).

To write to a writable stream, just call `.write(data)` with the data you want to write!




stream can plug the output of one stream to the input of another and use libraries that operate abstractly on streams to institute higher-level flow control.



## <span style='color: #3A539B; font-size:1.3em;'>	___Useful method___ </span> ##
---
#### _MODULE fs_ ####
1. `fs.readFile(file[, options], callback) | fs.readFile()` :



## <span style='color: #3A539B; font-size:1.5em;'>	___Important things___ </span> ##
#### _Hypertext Transfer Protocol(HTTP)_ ####
HTTP functions as a request–response protocol in the client–server computing model.

1. The client submits an HTTP request message to the server.
2. The server, which provides resources, returns a response message to the client

HTTP defines methods (sometimes referred to as verbs) to indicate the desired action to be performed on the identified resource.

`GET`


    The GET method requests a representation of the specified resource. Requests using GET should only retrieve data and should have no other effect. (This is also true of some other HTTP methods.)[1] The W3C has published guidance principles on this distinction, saying, "Web application design should be informed by the above principles, but also by the relevant limitations."[13] See safe methods below.


`HEAD`


    The HEAD method asks for a response identical to that of a GET request, but without the response body. This is useful for retrieving meta-information written in response headers, without having to transport the entire content.


`POST`


    The POST method requests that the server accept the entity enclosed in the request as a new subordinate of the web resource identified by the URI. The data POSTed might be, for example, an annotation for existing resources; a message for a bulletin board, newsgroup, mailing list, or comment thread; a block of data that is the result of submitting a web form to a data-handling process; or an item to add to a database.[14]


/*
*  (Exercise 7 of 13)
*  requête HTTP GET sur une URL
*/

var http = require('http')
var url = process.argv[2]

http.get(url, (response) => {
  response.setEncoding('utf-8')
  response.on('data', (data) => {
    console.log(data)
  })
})

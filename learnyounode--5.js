
/*
*  (Exercise 5 of 13)

*/
var fs = require('fs')

fs.readdir(process.argv[2], (errors, list) => {
  var extension = process.argv[3]
  var file = list.filter(i => i.includes('.' + extension.toString()))

  file.map(i => console.log(i))
})

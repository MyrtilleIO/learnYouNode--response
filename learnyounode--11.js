
/*
*  (Exercise 11 of 13)
*/

const http = require('http')
const fs = require('fs')
const port = process.argv[2]

const streamFile = fs.createReadStream(process.argv[3])

/* Création du serveur */
var server = http.createServer((req, res) => {
  streamFile.pipe(res)
})
server.listen(+port)

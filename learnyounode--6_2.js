
/*
*  (Exercise 5 of 13)

*/

var fs = require('fs')

module.exports=
function filt(path, extension, callback)
{
   fs.readdir(path, (error, list) => {
      if(error) {
         return callback(error)
      }
      var file = list.filter(i => i.includes('.' + extension.toString()))

      callback(null, file)
   })
}

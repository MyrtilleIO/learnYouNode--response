
/*
*  (Exercise 8 of 13)
*  requête HTTP GET sur une URL
*/

var http = require('http')
var concat = require('concat-stream')
var url = process.argv[2]

http.get(url, (response) => {
  response.pipe(concat((data) => {
    console.log(data.length)
    console.log(data.toString())
  }))
})

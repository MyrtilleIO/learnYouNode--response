
/*
*  (Exercise 10 of 13)
*/

const net = require('net')
const port = process.argv[2]


const server = net.createServer(function (socket) {
  // On ecrit la date sur le socket
  var date = new Date()

  var date1 = [date.getFullYear() + '-0', (date.getMonth() + 1) + '-', date.getDate()]
  var date2 = [' ' + date.getHours() + ':', date.getMinutes()]
  var date3 = date1.concat(date2).join('')
  socket.write(date3)
  socket.write('\n')
  socket.end()
})
server.listen(port)

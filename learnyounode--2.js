/*
*  (Exercise 2 of 13)
*  Write a program that accepts one or more numbers as command-line arguments
*  and prints the sum of those numbers to the console (stdout)
*
*  [DOC: nodeJS]
*  @process.argv ->
*  An array containing the command line arguments. The first element will be 'node',
*  the second element will be the name of the JavaScript file.
*  The next elements will be any additional command line arguments.
*/

var input = process.argv.filter(i => +i).map(i => +i).reduce((a, b) => a + b)
console.log(input)
